// Make sure to have 'boost_program_options' in your cmake,
//   or -lboost_program_options for g++.

#include <iostream>
#include <fstream>
#include <vector>
#include <boost/program_options.hpp>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

/// Parse arguments from blade command line input
bool static parseArguments(int argc,
                           char** argv,
                           int& delay,
                           uint32_t& readLength,
                           uint32_t& readSize,
                           std::string& path,
                           bool& continuous,
                           bool& listonly,
                           bool& verbose) {
    boost::program_options::options_description desc("This app will read all files from a given directory, into a buffer of a given size, and wait between files for a given delay.\nOptions");
    desc.add_options()("help,h", "This message\n");
    desc.add_options()("delay,d", boost::program_options::value<int>(&delay), "Delay between , in seconds.\n");
    desc.add_options()("readLength,l", boost::program_options::value<uint32_t>(&readLength), "Number of bytes to read before sleeping.  Must be larger than or equal to readsize.\n");
    desc.add_options()("readsize,r", boost::program_options::value<uint32_t>(&readSize), "Size of the bytes to read at a time (ram used) in Bytes.  Recommend 512 or more.\n");
    desc.add_options()("path,p", boost::program_options::value<std::string>(&path)->required(), "Path to read all files from.  This is Required.\n");
    desc.add_options()("continuous,c", boost::program_options::bool_switch(&continuous)->default_value(false), "After all files have been read from provided directory, do everything over again.\n");
    desc.add_options()("list-only,o", boost::program_options::bool_switch(&listonly)->default_value(false), "Don't do any reads or sleeps, just list out the files this program would have tried to read.\n");
    desc.add_options()("verbose,v", boost::program_options::bool_switch(&verbose)->default_value(false), "Don't output extra text, only information about if the app has looped in continuous mode.\n");
    try{
        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            return false;
        }

        boost::program_options::notify(vm);
    } catch (boost::program_options::error& e){
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return false;
    }

    return true;
}


int main(int argc, char** argv) {
    int delay = 0;// (seconds)
    uint32_t readLength = 1073741824;// (gigabyte)
    uint32_t readSize = 0;
    std::string path = "";
    bool continuous = false;
    bool listonly = false;
    bool verbose = false;
    if (!parseArguments(argc, argv, delay, readLength, readSize, path, continuous, listonly, verbose)) {
        return 3;
    }

    if(readLength < readSize){
        std::cerr << "ERROR: readLength not larger or equal to readSize." << std::endl;
        return 3;
    }

    //If no specific read size is provided, attempt to read the entirety of the read length
    //and let the operating system worry about how much to read at a time.
    if(readSize == 0){
        readSize = readLength;
    }

    do{
        std::cout << "Starting to read from the given directory: " << path << std::endl;
        std::vector<char> buffer(readSize);
        uint32_t bytesBeforeSleep = readLength;

        //Go through every file and try to read it.
        for (const auto & entry : fs::recursive_directory_iterator(path, fs::directory_options::skip_permission_denied)){
            if(verbose){
                std::cout << "Reading: " << entry.path() << " ..." << std::endl;
            }
            if(listonly){
                continue;
            }
            std::ifstream bigFile(entry.path());
            while (bigFile)
            {
                bigFile.read(buffer.data(), (readSize <= bytesBeforeSleep ? readSize : bytesBeforeSleep));
                bytesBeforeSleep -= bigFile.gcount();
                if(bytesBeforeSleep <= 0){
                    if(verbose){
                        std::cout << "Sleeping..." << std::endl;
                    }
                    sleep(delay);
                    bytesBeforeSleep = readLength;
                }
            }
            if(verbose){
                std::cout << "Finished reading: " << entry.path() << std::endl <<std::endl;
            }
        }

    } while (continuous);

    return 1;
}

